#!/usr/bin/ruby

require 'tumblr_client'
require 'json'
require 'net/http'
require 'digest/md5'
require 'reverse_markdown'
require 'fileutils'

#~ Tumblr API keys
Tumblr.configure do |config|
  config.consumer_key = "jm5J6ImnrndMKfvnA6avsDwrwlUUbKCWlwpnzRtEMai77c3H1Z"
  config.consumer_secret = "Zd5M5o5af96Kjdllb2uxzFmvdQ9JKhBf4JCUNhpSehdCEcTe9Q"
  config.oauth_token = "RV08BrIbtJP6oZXx49K1CNJeZsQYLaGBIcQ6nkVMOMaMgVTWT1"
  config.oauth_token_secret = "n6gDIrZNMf0nsnXijFPniCkxMQO6BiN7qY9gyMvVOT6Gw4JMdV"
end

#~ Base consts
BLOCK_SIZE = 25
BASE_LIKES = {"likes"=>0, "size"=>0, "posts"=>[]}

#~ Start the client
client = Tumblr::Client.new
#~ Get the number of liked posts
liked_count = client.likes["liked_count"]
#~ Where store the liked posts
likes = File.exists?("likes.json") ? JSON.parse(File.read("likes.json")) : BASE_LIKES

#~ Control
ids = []
for i in likes["posts"] do
  ids.push(i["slug"])
end
File.open(".control.json","w+"){|fo| fo.write(JSON.pretty_generate(ids))}

if likes["likes"] == liked_count then
  puts "I have no new liked posts."
else
  count = File.exists?("likes.json") ? liked_count - likes["likes"] : liked_count
  likes["likes"] = liked_count
  puts "I have #{count} new liked posts."
  tmp_posts = []
  tmp_size = 0

  for i in (0..count) do
    if i == 0 or i%BLOCK_SIZE == 0 then
      puts "I'll get #{BLOCK_SIZE} new posts starting on post ##{i}\tI have #{tmp_size} posts until now."
      #~ Get a new block
      for like in client.likes({:limit => BLOCK_SIZE, :offset => i})["liked_posts"] do
        #~ tmp
        tmp = {}
        #~ Verify if included
        if !ids.include?(like["id"]) then
          #~ Get "id" and the "slug"
          tmp["slug"] = like["slug"].empty? ? like["id"].to_s : like["slug"]

          #~ If is a photo
          if like["type"] == "photo" then
            if like["photos"].size >= 2 then
              tmp["blog"] = like["blog_name"]
              tmp["url"] = like["short_url"]
              tmp["cap"] = ReverseMarkdown.convert(like["cap"])
            end

            tmp["type"] = "photo"
            tmp["photos"] = []

            #~ For each "photo" on the post
            for photo in like["photos"] do
              #~ Get the "url"
              tmp["photos"].push(photo["original_size"]["url"])
            end

            #~ Store the data
            tmp_posts.push(tmp)

          #~ If it is a videos
          elsif like["type"] == "video" and like.key?("video_url") then
            tmp["type"] = "video"
            #~ Get the video URL
            tmp["video_url"] = like["video_url"]

            #~ Store the data
            tmp_posts.push(tmp)

          #~ If it is a text
          elsif like["type"] == "text" then
            tmp["type"] = "text"
            #~ Get the title
            title = like["title"].nil? ? "# " + like["id"].to_s + "\n\n" : "# " + like["title"] + "\n\n"
            #~ Get the body
            body = ReverseMarkdown.convert(like["body"])
            #~ Create the footer
            footer = "\n_By: [" + like["blog_name"] + "](" + like["short_url"] + "   In: " + like["date"] + "_"
            #~ Mix all
            tmp["content"] = title + body + footer

            #~ Store
            tmp_posts.push(tmp)
          end

          #~ Icrese size
          tmp_size += 1
        end
      end
    end
  end

  likes["posts"] = tmp_posts + likes["posts"]

  #~ Update Size
  likes["size"] = likes["posts"].size
  #~ Wirte down
  File.open("likes.json","w"){ |fo| fo.write JSON.pretty_generate(likes) }
end
puts ""

#~ Create the directorys
Dir.mkdir("likes") unless Dir.exist?("likes")
Dir.chdir("likes")
Dir.mkdir("videos") unless Dir.exist?("videos")
Dir.mkdir("texts") unless Dir.exist?("texts")

#~ Control file
File.open(".download","w") unless File.exist?(".download")
download = IO.readlines(".download").each{ |i| i.chomp! } if File.exist?(".download")

for post in likes["posts"] do

  if post["type"] == "photo" then
    Dir.mkdir("photos") unless Dir.exist?("photos")
    Dir.mkdir("photos/images") unless Dir.exist?("photos/images")
    control_info = false

    a = b = ""
    md5 = Digest::MD5.hexdigest(post['slug'])
    if post["photos"].size >= 2 then
      Dir.mkdir("photos/albuns") unless Dir.exist?("photos/albuns")

      a = md5.split(//).first(1).join("")
      Dir.mkdir("photos/albuns/#{a}") unless Dir.exist?("photos/albuns/#{a}")
      b = md5.split(//).first(2).last(1).join("")
      Dir.mkdir("photos/albuns/#{a}/#{b}") unless Dir.exist?("photos/albuns/#{a}/#{b}")

      if !Dir.exist?("photos/albuns/#{a}/#{b}/#{md5}") then
        Dir.mkdir("photos/albuns/#{a}/#{b}/#{md5}")
        control_info = true
        File.open("photos/albuns/#{a}/#{b}/#{md5}/info.md","a+"){|fo| fo.write "# #{md5}\n\n"}
      end
    end

    for ph in post["photos"] do
      if download.include?(ph) then
        print "%"
      else
        #~ Get the file
        r = Net::HTTP.get_response(URI.parse(ph))
        #~ Verify HTTP Code
        if r.code == "200" then
          #~ Find the extension
          ext = ph.split(//).last(3).join("")
          #~ Write the file
          File.open(".tmp","w"){ |fo| fo.write(r.body) }

          #~ If is an album
          if post["photos"].size >= 2 then
            #~ Find the index
            index = "%02d" % (post["photos"].index(ph)+1).to_s
            #~ Rename file
            name = Digest::MD5.hexdigest(File.read(".tmp")) + ".#{ext}"
            File.rename(".tmp", name)
            #~ Move File
            FileUtils.mv(name, "photos/albuns/#{a}/#{b}/#{md5}/#{name}")

            File.open("photos/albuns/#{a}/#{b}/#{md5}/info.md","a+"){ |fo|
              fo.write "![#{index}](#{name})\n"
            }

          #~ If not
          else
            #~ Find the MD5
            md5 = Digest::MD5.hexdigest(File.read(".tmp"))
            #~ Rename file
            name = "#{md5}.#{ext}"
            File.rename(".tmp", name)
            #~ Find where store
            a = md5.split(//).first(1).join("")
            Dir.mkdir("photos/images/#{a}") unless Dir.exist?("photos/images/#{a}")
            b = md5.split(//).first(2).last(1).join("")
            Dir.mkdir("photos/images/#{a}/#{b}") unless Dir.exist?("photos/images/#{a}/#{b}")
            #~ Move file
            FileUtils.mv(name, "photos/images/#{a}/#{b}/#{name}")
          end

          #~ Include URL
          File.open(".download","a"){ |fo| fo.puts ph }
          print "."
        else
          print "X"
        end
      end
    end

    File.open("photos/albuns/#{a}/#{b}/#{md5}/info.md","a+"){ |fo|
      fo.write "\n"
      fo.write post["cap"]
      fo.write "\n"
      fo.write "> [#{post['blog']}](#{post['url']})\n"
      fo.close()
    } if control_info
  elsif post["type"] == "video"
    if !download.include?(post["slug"]) then
      r = Net::HTTP.get_response(URI.parse(post["video_url"]))
      if r.code == "200" then
        ext = post["video_url"].split(//).last(3).join("")
        name = "#{post['slug']}.#{ext}"

        File.open("videos/#{name}","w"){|fo| fo.write(r.body)}

        File.open(".download","a"){ |fo| fo.puts post["slug"] }
        print "."
      end
    else
      print "%"
    end
  elsif post["type"] == "text"
    if !download.include?(post["slug"]) then
      File.open("texts/#{post["slug"]}.md","w"){|fo| fo.puts post["content"]}
    else
      print "%"
    end
  end
end

puts "\n\n"
puts "Done :3"
